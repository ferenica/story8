from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *

# Create your tests here.
class Story8_Web_Test(TestCase):
    
    def test_story8_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_using_landing_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_story8_using_home_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, home)
    
    def test_words(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Buku apa yang ingin kamu cari?', html_response)



