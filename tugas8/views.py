from django.shortcuts import render
from django.http import JsonResponse
from .forms import SearchForm
import requests

# Create your views here.
def home(request):
    response = {'search_form':SearchForm}
    return render(request, 'books.html', response)

def book_search(request):
    book_input = request.GET.get('book', None)
    if book_input == None:
        book_input=''

    url = 'https://www.googleapis.com/books/v1/volumes?q=' + book_input
    result = requests.get(url).json()

    return JsonResponse(result)

