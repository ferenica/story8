from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .views import *

# Create your tests here.
class Story9_Unit_Test(TestCase):
    
    def test_home_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def test_story9_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_story9_login_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_story_9_using_loginView_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginView)

    def test_story9_login_used_right_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_story9_url_logout_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 200)

    def test_story_9_using_logoutView_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logoutView)

    def test_story9_login_used_right_template(self):
        response = Client().get('/logout/')
        self.assertTemplateUsed(response, 'logout.html')

    def test_page_is_not_exist(self):
	#def test_page_is_not_exist(self):
        self.response = Client().get('/NotExistPage/')
        self.assertEqual(self.response.status_code, 404)

