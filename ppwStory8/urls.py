"""ppwStory8 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from tugas8.views import home, book_search
from django.contrib.auth import views as log_views
from tugas9.views import index, loginView, logoutView


urlpatterns = [
	path('admin/', admin.site.urls),
	path('', index, name="index"),
    path('books/', home, name = 'home'),
    
    path('book_api/', book_search, name = 'book_search'),
    path('login/', loginView, name="login"),
    path('logout/', logoutView, name="logout")

]
