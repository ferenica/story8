$(document).ready(function(){
    getBook();

    $('#search-bar').on('submit', '#input-form', function (event) {
        event.preventDefault(); //disable auto refresh when submitted
        getBook();
    })

})

function getBook() {
    var book = $("#search-input").val();

    if (typeof book === 'undefined' || book == '') {
        book = 'mango'
    }

    var complete_text = '';

    $.ajax({
        url:'/book_api/',
        data:{
            'book':book
        },
        success:function (result) {
            if ($.trim($("#hasil_list").html()).length != 0 ) {
                $("#hasil_list").empty();
            }

            for (i = 0; i < result.items.length; i++) {
                var book_title = result.items[i].volumeInfo.title;
                var book_thumbnail = result.items[i].volumeInfo.imageLinks;
                var book_authors = result.items[i].volumeInfo.authors;
                var book_publisher = result.items[i].volumeInfo.publisher;
                var book_alt_thumbnail = '';

                if (typeof book_title === 'undefined') {
                    book_title = 'Judul tidak ditemukan'
                }

                if (typeof book_thumbnail === 'undefined') {
                    book_thumbnail = '#'
                    book_alt_thumbnail = 'Gambar tidak ditemukan'
                } else {
                    book_thumbnail = book_thumbnail.thumbnail;
                }

                if (typeof book_publisher === 'undefined') {
                    book_publisher = 'Penerbit tidak ditemukan'
                }

                if (typeof book_authors === 'undefined') {
                    book_authors = 'Penulis tidak ditemukan'
                } else {
                    book_authors = book_authors.join(' - ')
                }

                complete_text += '<tr>'+
                    '<th scope="row">'+(i+1)+'</th>'+
                    '<td><img src="'+book_thumbnail+'" alt="'+book_alt_thumbnail+'"></td>'+
                    '<td>'+book_title+'</td>'+
                    '<td>'+book_authors+'</td>'+
                    '<td>'+book_publisher+'</td>'+
                    '</tr>'
            }

            $("#hasil_list").append(
                '<table class="table table-light table-bordered table-responsive"><thead>'+
            '<tr>'+
                '<th scope="col">No.</th>'+
                '<th scope="col">Gambar</th>'+
                '<th scope="col">Judul</th>'+
                '<th scope="col">Penulis</th>'+
                '<th scope="col">Penerbit</th>'+
            '</tr></thead>'+complete_text+"</table>")

            book = ''
        }
        
    })
}
